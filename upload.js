//import require modules
const multer = require('multer');
const xlsxtojson=require("xlsx-to-json-lc");
const xlstojson=require("xls-to-json-lc");
const exp=require('express');
let app = exp();

const bodyparser=require('body-parser');

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());

//multers disk storage settings
var storage = multer.diskStorage({
destination: function (req, file, cb) {
cb(null, './uploads/')
},
filename: function (req, file, cb) {

cb(null, `${new Date().getTime()}_${file.originalname}`)
}
});
// upload middleware
const upload = multer({ storage: storage});
// convert excel to json route
app.post("/exam",upload.single('uploadfile'),(req,res)=>{
if(req.file.originalname.split('.')[req.file.originalname.split('.').length-1] === 'xlsx'){
exceltojson = xlsxtojson;
} else {
exceltojson = xlstojson;
}
try {
exceltojson({
input: req.file.path, //the same path where we uploaded our file
output: null, //since we don't need output.json
lowerCaseHeaders:true
}, function(err,result){
if(err) {
return res.json({error_code:1,err_desc:err, data: null});
}
// getDB().collection("attendence").insertMany(result, (err, data) => {
// console.log(data);
// res.json({error_code:0,err_desc:null, data:
// data["ops"],"message":"Attendence Sheet uploaded successfully"});
// });
res.send(result)
});
} catch (e){
res.json({error_code:1,err_desc:"Corupted excel file"});
}
});


app.listen(3000,()=>{
    console.log("server start");
    
})